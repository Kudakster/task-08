package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.enity.Flower;

import javax.xml.transform.TransformerException;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
	private static List<Flower> flowerList = new LinkedList<>();
	private static String outputXmlFile = null;

	public static void main(String[] args) throws Exception {
		String xmlFileName = "input.xml";

		parseDOM(xmlFileName);
		parseSAX(xmlFileName);
		parseStAX(xmlFileName);
	}

	private static void parseDOM(String xmlFileName) throws TransformerException {
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get container
		Controller domController = new DOMController(xmlFileName);
		flowerList = domController.parseFile();

		// sort (case 1)
		flowerList.sort(Comparator.comparing(Flower::getName));

		// save
		String outputXmlFile = "output.dom.xml";
		Convertor.save(flowerList, outputXmlFile);
	}

	private static void parseSAX(String xmlFileName) throws TransformerException {
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		// get
		Controller saxController = new SAXController(xmlFileName);
		flowerList = saxController.parseFile();

		// sort  (case 2)
		try {
			flowerList = flowerList.stream()
					.sorted(Comparator.comparing(o -> o.getVisualParameters().getAveLenFlower())).collect(Collectors.toList());
		} catch (NullPointerException exception) {
			System.out.println("Can not sort because the value = null");
		}

		// save
		outputXmlFile = "output.sax.xml";
		Convertor.save(flowerList, outputXmlFile);
	}

	private static void parseStAX(String xmlFileName) throws TransformerException {
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		Controller staxController = new STAXController(xmlFileName);
		flowerList = staxController.parseFile();

		// sort  (case 2)
		try {
			flowerList = flowerList.stream()
					.sorted(Comparator.comparing(o -> o.getGrowingParameters().getTemperature())).collect(Collectors.toList());
		} catch (NullPointerException exception) {
			System.out.println("Can not sort because the value = null");
		}

		// save
		outputXmlFile = "output.stax.xml";
		Convertor.save(flowerList, outputXmlFile);
	}
}
