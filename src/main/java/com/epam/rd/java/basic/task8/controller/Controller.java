package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.enity.Flower;

import java.util.List;

public interface Controller {
    List<Flower> parseFile();
}
