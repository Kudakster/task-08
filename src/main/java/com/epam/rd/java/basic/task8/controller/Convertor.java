package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.enity.Flower;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

public interface Convertor {
    static void save(List<Flower> list, String outputXmlFile) throws TransformerException {
        Document document = null;

        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            document = documentBuilder.newDocument();

            Element root = document.createElementNS("http://www.nure.ua", "flowers");
            root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
            document.appendChild(root);

            for (Flower value : list) {
                Element flower = document.createElement("flower");
                root.appendChild(flower);

                Element name = document.createElement("name");
                name.appendChild(document.createTextNode(value.getName()));
                flower.appendChild(name);

                Element soil = document.createElement("soil");
                soil.appendChild(document.createTextNode(value.getSoil()));
                flower.appendChild(soil);

                Element origin = document.createElement("origin");
                origin.appendChild(document.createTextNode(value.getOrigin()));
                flower.appendChild(origin);

                Element visualParameters = document.createElement("visualParameters");
                flower.appendChild(visualParameters);

                Element stemColour = document.createElement("stemColour");
                stemColour.appendChild(document.createTextNode(value.getVisualParameters().getStemColour()));
                visualParameters.appendChild(stemColour);

                Element leafColour = document.createElement("leafColour");
                leafColour.appendChild(document.createTextNode(value.getVisualParameters().getLeafColour()));
                visualParameters.appendChild(leafColour);

                Element aveLenFlower = document.createElement("aveLenFlower");
                Attr measure = document.createAttribute("measure");
                measure.setValue("cm");
                aveLenFlower.setAttributeNode(measure);
                aveLenFlower.appendChild(document.createTextNode(value.getVisualParameters().getAveLenFlower()));
                visualParameters.appendChild(aveLenFlower);

                Element growingTips = document.createElement("growingTips");
                flower.appendChild(growingTips);

                Element temperature = document.createElement("temperature");
                Attr measureTemp = document.createAttribute("measure");
                measureTemp.setValue("celsius");
                temperature.setAttributeNode(measureTemp);
                temperature.appendChild(document.createTextNode(value.getGrowingParameters().getTemperature()));
                growingTips.appendChild(temperature);

                Element lighting = document.createElement("lighting");
                Attr lightRequiring = document.createAttribute("lightRequiring");
                lightRequiring.setValue(value.getGrowingParameters().getLight());
                lighting.setAttributeNode(lightRequiring);
                growingTips.appendChild(lighting);

                Element watering = document.createElement("watering");
                Attr measureWater = document.createAttribute("measure");
                measureWater.setValue("mlPerWeek");
                watering.setAttributeNode(measureWater);
                watering.appendChild(document.createTextNode(value.getGrowingParameters().getWatering()));
                growingTips.appendChild(watering);

                Element multiplying = document.createElement("multiplying");
                multiplying.appendChild(document.createTextNode(value.getMultiplying()));
                flower.appendChild(multiplying);
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        DOMSource domSource = new DOMSource(document);
        StreamResult streamResult = new StreamResult(new File(outputXmlFile));

        transformer.transform(domSource, streamResult);
    }
}
