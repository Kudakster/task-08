package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.enity.Flower;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController implements Controller {
    private final List<Flower> flowerList = new ArrayList<>();
    private final String xmlFileName;
    private String name = null;
    private String soil = null;
    private String origin = null;
    private String multiplying = null;
    private String stemColour = null;
    private String leafColour = null;
    private int aveLenFlower = 0;
    private int temperature = 0;
    private String light = null;
    private int watering = 0;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> parseFile() {
        Document document = getDocument();
        NodeList rootNodeList = document.getElementsByTagName("flower");

        for (int i = 0; i < rootNodeList.getLength(); i++) {
            NodeList nodeList = rootNodeList.item(i).getChildNodes();
            for (int j = 0; j < nodeList.getLength(); j++) {
                if (nodeList.item(j).getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }

                if (nodeList.item(j).getNodeName().equals("visualParameters") ||
                        nodeList.item(j).getNodeName().equals("growingTips")) {
                    NodeList childNodeList = nodeList.item(j).getChildNodes();
                    for (int k = 0; k < childNodeList.getLength(); k++) {
                        if (childNodeList.item(k).getNodeType() != Node.ELEMENT_NODE) {
                            continue;
                        }

                        switch (childNodeList.item(k).getNodeName()) {
                            case "stemColour": {
                                stemColour = childNodeList.item(k).getTextContent();
                                break;
                            }
                            case "leafColour": {
                                leafColour = childNodeList.item(k).getTextContent();
                                break;
                            }
                            case "aveLenFlower": {
                                aveLenFlower = Integer.parseInt(childNodeList.item(k).getTextContent());
                                break;
                            }
                            case "temperature": {
                                temperature = Integer.parseInt(childNodeList.item(k).getTextContent());
                                break;
                            }
                            case "lighting": {
                                light = childNodeList.item(k).getAttributes().getNamedItem("lightRequiring").getNodeValue();
                                break;
                            }
                            case "watering": {
                                watering = Integer.parseInt(childNodeList.item(k).getTextContent());
                                break;
                            }
                        }
                    }
                }

                switch (nodeList.item(j).getNodeName()) {
                    case "name": {
                        name = nodeList.item(j).getTextContent();
                        break;
                    }
                    case "soil": {
                        soil = nodeList.item(j).getTextContent();
                        break;
                    }
                    case "origin": {
                        origin = nodeList.item(j).getTextContent();
                        break;
                    }
                    case "multiplying": {
                        multiplying = nodeList.item(j).getTextContent();
                        break;
                    }
                }
            }
            flowerList.add(new Flower(name, soil, origin, multiplying, stemColour, leafColour, aveLenFlower, temperature, light, watering));
        }
        return flowerList;
    }

    private Document getDocument() {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document document = null;
        try {
            dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            document = dbf.newDocumentBuilder().parse(xmlFileName);
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
        return document;
    }
}
