package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.controller.handler.SAXParserHandler;
import com.epam.rd.java.basic.task8.enity.Flower;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler implements Controller {

    private final String xmlFileName;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public List<Flower> parseFile() {
        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        SAXParserHandler parserHandler = new SAXParserHandler();
        SAXParser parser;

        try {
            parserFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            parser = parserFactory.newSAXParser();
            parser.parse(xmlFileName, parserHandler);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return parserHandler.getFlowerList();
    }
}