package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.enity.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler implements Controller {
    private final List<Flower> flowerList = new ArrayList<>();
    private final String xmlFileName;
    private String name = null;
    private String soil = null;
    private String origin = null;
    private String multiplying = null;
    private String stemColour = null;
    private String leafColour = null;
    private int aveLenFlower = 0;
    private int temperature = 0;
    private String light = null;
    private int watering = 0;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public List<Flower> parseFile() {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            xmlInputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
            XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
            while (reader.hasNext()) {
                XMLEvent xmlEvent = reader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    switch (startElement.getName().getLocalPart()) {
                        case "name": {
                            name = reader.nextEvent().asCharacters().getData();
                            break;
                        }
                        case "soil": {
                            soil = reader.nextEvent().asCharacters().getData();
                            break;
                        }
                        case "origin": {
                            origin = reader.nextEvent().asCharacters().getData();
                            break;
                        }
                        case "multiplying": {
                            multiplying = reader.nextEvent().asCharacters().getData();
                            break;
                        }
                        case "stemColour": {
                            stemColour = reader.nextEvent().asCharacters().getData();
                            break;
                        }
                        case "leafColour": {
                            leafColour = reader.nextEvent().asCharacters().getData();
                            break;
                        }
                        case "aveLenFlower": {
                            aveLenFlower = Integer.parseInt(reader.nextEvent().asCharacters().getData());
                            break;
                        }
                        case "temperature": {
                            temperature = Integer.parseInt(reader.nextEvent().asCharacters().getData());
                            break;
                        }
                        case "watering": {
                            watering = Integer.parseInt(reader.nextEvent().asCharacters().getData());
                            break;
                        }
                        case "lighting": {
                            Attribute lightRequiringAttr = startElement.getAttributeByName(new QName("lightRequiring"));
                            if (lightRequiringAttr != null) {
                                light = lightRequiringAttr.getValue();
                            }
                        }
                    }
                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("flower")) {
                        flowerList.add(new Flower(name, soil, origin, multiplying, stemColour, leafColour, aveLenFlower, temperature, light, watering));
                    }
                }
            }
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return flowerList;
    }
}
