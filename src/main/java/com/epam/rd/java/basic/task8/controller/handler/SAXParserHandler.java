package com.epam.rd.java.basic.task8.controller.handler;

import com.epam.rd.java.basic.task8.enity.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXParserHandler extends DefaultHandler {
    private final List<Flower> flowerList = new ArrayList<>();
    private String currentTagName;
    private String name = null;
    private String soil = null;
    private String origin = null;
    private String multiplying = null;
    private String stemColour = null;
    private String leafColour = null;
    private int aveLenFlower = 0;
    private int temperature = 0;
    private String light = null;
    private int watering = 0;

    public List<Flower> getFlowerList() {
        return flowerList;
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
    }

    @Override
    public void endDocument() throws SAXException {
        super.endDocument();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        currentTagName = qName;

        if (qName.equals("lighting")) {
            light = attributes.getValue("lightRequiring");
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (currentTagName != null && currentTagName.equals("multiplying")) {
            flowerList.add(new Flower(name, soil, origin, multiplying, stemColour, leafColour, aveLenFlower, temperature, light, watering));
        }

        currentTagName = null;
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        if (currentTagName == null) {
            return;
        }

        switch (currentTagName) {
            case "name": {
                name = new String(ch, start, length);
                break;
            }
            case "soil": {
                soil = new String(ch, start, length);
                break;
            }
            case "origin": {
                origin = new String(ch, start, length);
                break;
            }
            case "multiplying": {
                multiplying = new String(ch, start, length);
                break;
            }
            case "stemColour": {
                stemColour = new String(ch, start, length);
                break;
            }
            case "leafColour": {
                leafColour = new String(ch, start, length);
                break;
            }
            case "aveLenFlower": {
                aveLenFlower = Integer.parseInt(new String(ch, start, length));
                break;
            }
            case "temperature": {
                temperature = Integer.parseInt(new String(ch, start, length));
                break;
            }
            case "watering": {
                watering = Integer.parseInt(new String(ch, start, length));
                break;
            }
        }
    }
}
