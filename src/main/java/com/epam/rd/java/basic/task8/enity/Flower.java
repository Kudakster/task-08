package com.epam.rd.java.basic.task8.enity;

import com.epam.rd.java.basic.task8.enity.complex_paramaters.GrowingParameters;
import com.epam.rd.java.basic.task8.enity.complex_paramaters.VisualParameters;
import com.epam.rd.java.basic.task8.enity.enumaration.Multiplying;
import com.epam.rd.java.basic.task8.enity.enumaration.Soil;

import java.util.Arrays;

public class Flower {
    private final String name;
    private final String soil;
    private final String origin;
    private final String multiplying;
    private final VisualParameters visualParameters;
    private final GrowingParameters growingParameters;

    public Flower(String name, String soil, String origin, String multiplying, String stemColour, String leafColour, int aveLenFlower, int temperature, String requiring, int watering) {
        this.name = name;
        this.soil = getSoilType(soil);
        this.origin = origin;
        this.multiplying = getMultiplyingType(multiplying);
        this.visualParameters = new VisualParameters(stemColour, leafColour, aveLenFlower);
        this.growingParameters = new GrowingParameters(temperature, requiring, watering);
    }

    private String getSoilType(String soil) {
        if (Arrays.stream(Soil.values())
                .map(Soil::getType)
                .anyMatch(s -> s.equals(soil))) {
            return soil;
        } else return null;
    }

    private String getMultiplyingType(String multiplying) {
        if (Arrays.stream(Multiplying.values())
                .map(Multiplying::getMultiplying)
                .anyMatch(m -> m.equals(multiplying))) {
            return multiplying;
        } else return null;
    }

    public String getName() {
        return name;
    }

    public String getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public GrowingParameters getGrowingParameters() {
        return growingParameters;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", multiplying='" + multiplying + '\'' +
                ", visualParameters=" + visualParameters +
                ", growingParameters=" + growingParameters +
                '}';
    }
}
