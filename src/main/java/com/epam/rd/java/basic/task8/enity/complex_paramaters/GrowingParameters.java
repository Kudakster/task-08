package com.epam.rd.java.basic.task8.enity.complex_paramaters;

import com.epam.rd.java.basic.task8.enity.enumaration.LightRequiring;

import java.util.Arrays;

public class GrowingParameters {
    private final String temperature;
    private final String light;
    private final String watering;

    public GrowingParameters(int temperature, String requiring, int watering) {
        this.temperature = setTemperature(temperature);
        this.light = getLightRequiring(requiring);
        this.watering = setWatering(watering);
    }

    private String setTemperature(int temp) {
        if (temp <= 0) {
            return null;
        } else {
            return temp + " celsius";
        }
    }

    private String getLightRequiring(String requiring) {
        if (Arrays.stream(LightRequiring.values())
                .map(LightRequiring::getRequiring)
                .anyMatch(r -> r.equals(requiring))) {
            return requiring;
        } else return null;
    }

    private String setWatering(int water) {
        if (water <= 0) {
            return null;
        } else {
            return water + " mlPerWeek";
        }
    }

    public String getTemperature() {
        if (temperature == null) {
            return null;
        } else return temperature.replaceAll("[^0-9]", "");
    }

    public String getLight() {
        return light;
    }

    public String getWatering() {
        if (watering == null) {
            return null;
        } else return watering.replaceAll("[^0-9]", "");
    }

    @Override
    public String toString() {
        return "GrowingParameters{" +
                "temperature='" + temperature + '\'' +
                ", light='" + light + '\'' +
                ", watering='" + watering + '\'' +
                '}';
    }
}


