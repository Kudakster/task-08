package com.epam.rd.java.basic.task8.enity.complex_paramaters;

public class VisualParameters {
    private final String stemColour;
    private final String leafColour;
    private final String aveLenFlower;

    public VisualParameters(String stemColour, String leafColour, int aveLenFlower) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = setAveLenFlower(aveLenFlower);
    }

    private String setAveLenFlower(int length) {
        if (length <= 0) {
            return null;
        } else {
            return length + " cm";
        }
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public String getAveLenFlower() {
        if (aveLenFlower == null) {
            return null;
        } else return aveLenFlower.replaceAll("[^0-9]", "");
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "steamColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower='" + aveLenFlower + '\'' +
                '}';
    }
}
