package com.epam.rd.java.basic.task8.enity.enumaration;

public enum LightRequiring {
    YES("yes"), NO("no");

    private final String requiring;

    LightRequiring(String requiring) {
        this.requiring = requiring;
    }

    public String getRequiring() {
        return requiring;
    }
}
