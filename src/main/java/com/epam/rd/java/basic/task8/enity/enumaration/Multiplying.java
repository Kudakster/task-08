package com.epam.rd.java.basic.task8.enity.enumaration;

public enum Multiplying {
    LEAVES("листья"), CUTTINGS("черенки"), SEEDS("семена");

    private final String multiplying;

    Multiplying(String type) {
        this.multiplying = type;
    }

    public String getMultiplying() {
        return multiplying;
    }
}
