package com.epam.rd.java.basic.task8.enity.enumaration;

public enum Soil {
    PODZOLIK("подзолистая"), UNPAVED("грунтовая"), SOD_PODZOLIC("дерново-подзолистая");

    private final String type;

    Soil(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
